#include "bouncyballwindow.h"

BouncyBallWindow::BouncyBallWindow(QWidget *parent)
    : QMainWindow(parent)
{
    const QVector2D initial_resolution{640, 480};
    this->setWindowTitle("Kulki");
    this->resize(initial_resolution.x(), initial_resolution.y());
    this->main_widget = std::unique_ptr<QWidget>(new QWidget(this));
    this->layout = std::unique_ptr<QVBoxLayout>(new QVBoxLayout(this->main_widget.get()));

    this->control_panel = std::unique_ptr<ControlPanel>(new ControlPanel(this));
    this->screen = new Screen(initial_resolution);

    this->layout.get()->addWidget(this->screen, 1);
    this->layout.get()->addWidget(this->control_panel.get(), 0);

    this->connect_signals();

    this->setCentralWidget(this->main_widget.get());
    this->activateWindow();
    this->show();

    this->refresh_timer = std::unique_ptr<QTimer>(new QTimer());
    connect(this->refresh_timer.get(), SIGNAL(timeout()), this->screen, SLOT(new_frame()));
    this->refresh_timer.get()->start(1000/FPS);
}

BouncyBallWindow::~BouncyBallWindow()
{
    delete screen;
}

void BouncyBallWindow::connect_signals()
{
    Screen* screen = this->screen;

    QSlider& speed_one_slider = control_panel->get_speed_one_slider();
    speed_one_slider.setValue(screen->get_ball(0)->get_speed());
    QSlider& speed_two_slider = control_panel->get_speed_two_slider();
    speed_two_slider.setValue(screen->get_ball(1)->get_speed());

    connect(&speed_one_slider, &QSlider::valueChanged, screen, [screen](int slider_value){screen->set_ball_speed(0, slider_value);});
    connect(&speed_two_slider, &QSlider::valueChanged, screen, [screen](int slider_value){screen->set_ball_speed(1, slider_value);});

    connect((screen->get_ball(0).get()), &Ball::speed_updated, &speed_one_slider, [&speed_one_slider](float speed){
        bool blocking = speed_one_slider.blockSignals(true);
        speed_one_slider.setValue(static_cast<int>(speed));
        speed_one_slider.blockSignals(blocking);
    });
    connect((screen->get_ball(1).get()), &Ball::speed_updated, &speed_two_slider, [&speed_two_slider](float speed){
        bool blocking = speed_two_slider.blockSignals(true);
        speed_two_slider.setValue(static_cast<int>(speed));
        speed_two_slider.blockSignals(blocking);
    });

    QSlider& size_one_slider = control_panel->get_size_one_slider();
    QSlider& size_two_slider = control_panel->get_size_two_slider();

    size_one_slider.setValue(screen->get_ball(0)->get_radius_percentage());
    size_two_slider.setValue(screen->get_ball(1)->get_radius_percentage());
    connect(&size_one_slider, &QSlider::valueChanged, screen, [screen](int slider_value){screen->set_ball_size(0, slider_value);});
    connect(&size_two_slider, &QSlider::valueChanged, screen, [screen](int slider_value){screen->set_ball_size(1, slider_value);});
}

