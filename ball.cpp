#include "ball.h"
float Ball::max_radius = 1.0f;

Ball::Ball(QVector2D& resolution): QObject(nullptr)
{
    update_max_radius(resolution);
    circumference = get_random_circumference();
    position = get_random_position(resolution, circumference);
    set_velocity(get_random_direction() * get_random_speed());
}

const QVector2D &Ball::get_position()
{
    return position;
}

const QVector2D &Ball::get_velocity()
{
    return velocity;
}

void Ball::set_position(const QVector2D &position)
{
    this->position = position;
}

void Ball::set_velocity(const QVector2D &velocity)
{
    this->velocity = velocity;
    emit speed_updated(this->get_speed());
}

float Ball::get_size()
{
    return circumference;
}

float Ball::get_radius()
{
    return circumference/2.0f;
}

int Ball::get_radius_percentage() const
{
    return static_cast<int>(radius_magnitude * 100);
}

void Ball::move(float delta_time)
{
    position += velocity * delta_time/1000;
}

std::array<float, 3> Ball::get_float_array()
{
    return {position[0], position[1], circumference};
}

void Ball::update_max_radius(QVector2D& new_resolution)
{
    Ball::max_radius = std::min(new_resolution[0], new_resolution[1]) * 0.3;
}

void Ball::recalculate_radius()
{
    circumference = Ball::min_radius + (Ball::max_radius - Ball::min_radius) * radius_magnitude;
}

void Ball::set_radius_percentage(int percentage)
{
    radius_magnitude = percentage / 100.0f;
    recalculate_radius();
}

void Ball::set_speed(int percentage)
{
    velocity = velocity.normalized() * percentage;
}

void Ball::flip_velocity(size_t index)
{
    velocity[index] *= -1.0f;
}

QVector2D Ball::get_random_position(QVector2D resolution, int radius)
{
//    static std::random_device random_device;
    // Because Windows' "random device" is a crock of shit, and equivalent to this:
    // https://xkcd.com/221/
    static std::mt19937 random_engine = std::mt19937(std::chrono::high_resolution_clock::now().time_since_epoch().count());

    std::uniform_int_distribution<int> x_distribution(radius, static_cast<int>(resolution.x() - radius));
    std::uniform_int_distribution<int> y_distribution(radius, static_cast<int>(resolution.y() - radius));

    auto random_position = QVector2D(x_distribution(random_engine), y_distribution(random_engine));

    return random_position;
}

float Ball::get_random_circumference()
{
    static std::mt19937 random_engine = std::mt19937(std::chrono::high_resolution_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<int> circumference_dist(min_radius, max_radius);
    return circumference_dist(random_engine);
}

QVector2D Ball::get_random_direction()
{
    static std::mt19937 random_engine = std::mt19937(std::chrono::high_resolution_clock::now().time_since_epoch().count());
    std::uniform_real_distribution<float> random_in_dimension(0.0f, 1.0f);
    return QVector2D(random_in_dimension(random_engine), random_in_dimension(random_engine));
}

float Ball::get_random_speed()
{
    static std::mt19937 random_engine = std::mt19937(std::chrono::high_resolution_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<int> speed_dist(Ball::min_speed, Ball::max_speed);
    return speed_dist(random_engine);
}

float Ball::get_speed()
{
    return velocity.length();
}
