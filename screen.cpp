#include "screen.h"


Screen::Screen(QVector2D resolution): QOpenGLWidget(), resolution(resolution)
{
    Ball* ball_one = new Ball{resolution};
    balls.push_back(std::unique_ptr<Ball>(ball_one));
    Ball* ball_two = new Ball{resolution};
    balls.push_back(std::unique_ptr<Ball>(ball_two));
}

QVector2D Screen::pixels_to_gl_coord(const QVector2D& pixels)
{
    return QVector2D(-1.0f + pixels.x()*2 / resolution[0], -1.0f + pixels.y() * 2 / resolution[1]);
}

void Screen::new_frame()
{
    physics.update();
    this->update();
}

std::unique_ptr<Ball>& Screen::get_ball(int index)
{
    return balls[index];
}

void Screen::set_ball_speed(int index, int speed_magnitude)
{
    balls[index]->set_speed(speed_magnitude);
}

void Screen::set_ball_size(int index, int size)
{
    balls[index]->set_radius_percentage(size);
}

void Screen::initializeGL()
{
    makeCurrent();

    opengl = QOpenGLContext::currentContext()->functions();
    opengl->initializeOpenGLFunctions();
    opengl->glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    opengl->glEnable(GL_PROGRAM_POINT_SIZE);
    opengl->glEnable(GL_POINT_SPRITE);
    opengl->glEnable(GL_BLEND);
    opengl->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    init_ball_shader();
}

void Screen::paintGL()
{
    const int ball_count = balls.size();
    const int elements_per_ball = 3;
    std::vector<float> vertex_data;
    vertex_data.reserve(elements_per_ball * ball_count);

    for (size_t ball_i=0, element_i=0; ball_i<ball_count; ++ball_i){
        const QVector2D& ball_position = pixels_to_gl_coord(balls[ball_i]->get_position());
        const float radius = balls[ball_i]->get_size();
        const std::array<float, 3> ball_array{ball_position.x(), ball_position.y(), radius};

//        auto ball_array = balls[ball_i].get_float_array();
        std::copy(ball_array.begin(), ball_array.end(), vertex_data.begin()+element_i);
        element_i += ball_array.size();
    }

//    auto ball_array = balls[0].get_float_array();
//    std::copy(ball_array.begin(), ball_array.end(), test_vert[ball_array.size()]);

    ball_buffer.write(0, vertex_data.data(), sizeof(vertex_data));

    opengl->glDrawArrays(GL_POINTS, 0, ball_count);
}

void Screen::resizeGL(int w, int h)
{
    resolution = QVector2D(w, h);
    Ball::update_max_radius(resolution);
    for (auto& ball : balls){
        ball->recalculate_radius();
    }
}

void Screen::init_ball_shader()
{
    static const QString VERTEX_SHADER_PATH = "./ball_shader.vs";
    static const QString FRAGMENT_SHADER_PATH = "./ball_shader.fs";

    q_shader_program = new QOpenGLShaderProgram(this);  // delete?
    q_shader_program->addShaderFromSourceFile(QOpenGLShader::Vertex, VERTEX_SHADER_PATH);
    q_shader_program->addShaderFromSourceFile(QOpenGLShader::Fragment, FRAGMENT_SHADER_PATH);
    q_shader_program->link();
    q_shader_program->bind();
    circle_center = q_shader_program->attributeLocation("qt_Vertex");
    circle_radius = q_shader_program->attributeLocation("circle_radius");

    ball_buffer.create();
    ball_buffer.bind();
    ball_buffer.allocate(3*3*sizeof(float));
    ball_buffer.setUsagePattern(QOpenGLBuffer::UsagePattern::DynamicDraw);

    opengl->glVertexAttribPointer(circle_center, 2, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
    opengl->glEnableVertexAttribArray(circle_center);
    opengl->glVertexAttribPointer(circle_radius, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)(2*sizeof(float)));
    opengl->glEnableVertexAttribArray(circle_radius);
}

std::string Screen::get_file_text(const std::string &filename)
{
    std::ifstream shader_file{filename};
    std::stringstream shader_stream{};
    shader_stream << shader_file.rdbuf();
    std::string file_text = shader_stream.str();
    shader_file.close();
    return file_text;
}
