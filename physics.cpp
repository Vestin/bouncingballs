#include "physics.h"

Physics::Physics(std::vector<std::unique_ptr<Ball>>& balls, QVector2D& resolution): balls(balls), resolution(resolution)
{
    last_time = std::chrono::steady_clock::now();
}

void Physics::update()
{
    auto now = std::chrono::steady_clock::now();
    auto delta_time_duration = now - last_time;
    last_time = now;
    auto delta_time = std::chrono::duration_cast<std::chrono::milliseconds>(delta_time_duration).count();

    for (auto& ball : balls){
        ball->move(delta_time);
        wall_bounce(*ball);
    }

    if (do_balls_collide(*balls[0], *balls[1])){
        resolve_collision(*balls[0], *balls[1]);
    }

}

void Physics::wall_bounce(Ball &ball)
{
    for (int dimension=0; dimension<2; ++dimension){
        bool flip = false;
        float position = ball.position[dimension];
        float radius = ball.get_radius();
        float min_edge = position - radius;
        float max_edge = position + radius;
        if (min_edge < 0.0f){
            ball.position[dimension] = position-(min_edge - 0.0f);
            flip = true;
        } else if (max_edge > resolution[dimension]){
            ball.position[dimension] = position-(max_edge - resolution[dimension]);
            flip = true;
        }
        if (flip){
            ball.flip_velocity(dimension);
        }
    }
}

float Physics::get_adjusted_ball_radius(Ball &ball)
{
    return ball.get_size() / std::min(resolution[0], resolution[1]);
}

bool Physics::do_balls_collide(Ball &ball_one, Ball &ball_two)
{
    // Can optimize with distance squared used instead
    float radius_one = ball_one.get_radius();
    float radius_two = ball_two.get_radius();
    float distance = std::hypot(ball_one.position[0] - ball_two.position[0], ball_one.position[1] - ball_two.position[1]);
    return distance < (radius_one + radius_two);
}

void Physics::resolve_collision(Ball &ball_one, Ball &ball_two)
{
    QVector2D ball_one_v = ball_one.get_velocity();
    QVector2D ball_two_v = ball_two.get_velocity();

    QVector2D line_of_penetration = ball_two.get_position() - ball_one.get_position();
    QVector2D penetration_normalized = line_of_penetration.normalized();
    float penetration_depth_half = ((ball_one.get_radius() + ball_two.get_radius()) - line_of_penetration.length()) / 2.0f;

    QVector2D one_v_along = penetration_normalized * QVector2D::dotProduct(ball_one_v, penetration_normalized);
    QVector2D two_v_along = penetration_normalized * QVector2D::dotProduct(ball_two_v, penetration_normalized);

    QVector2D one_v_perp = ball_one_v - one_v_along;
    QVector2D two_v_perp = ball_two_v - two_v_along;

    ball_one.set_velocity(one_v_perp + two_v_along);
    ball_two.set_velocity(two_v_perp + one_v_along);

    QVector2D new_position_one = ball_one.position - (penetration_normalized * penetration_depth_half);
    QVector2D new_position_two = ball_two.position + (penetration_normalized * penetration_depth_half);

    ball_one.set_position(new_position_one);
    ball_two.set_position(new_position_two);
}
