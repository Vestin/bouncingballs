#ifndef BOUNCYBALLWINDOW_H
#define BOUNCYBALLWINDOW_H

#include "screen.h"
#include "controlpanel.h"

#include <QMainWindow>
#include <QTimer>
#include <QVBoxLayout>

class BouncyBallWindow : public QMainWindow
{
    Q_OBJECT

public:
    BouncyBallWindow(QWidget *parent = nullptr);
    ~BouncyBallWindow();

private:
    std::unique_ptr<QWidget> main_widget;
    std::unique_ptr<QVBoxLayout> layout;
    Screen* screen;
    std::unique_ptr<ControlPanel> control_panel;

    const int FPS = 60;
    std::unique_ptr<QTimer> refresh_timer;

    void connect_signals();
};
#endif // BOUNCYBALLWINDOW_H
