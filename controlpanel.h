#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H

#include <QWidget>
#include <QGridLayout>
#include <QSlider>
#include <memory>
#include <QLabel>

#include "ball.h"

class ControlPanel : public QWidget
{
    Q_OBJECT

public:
    explicit ControlPanel(QWidget *parent = nullptr);
    QSlider& get_speed_one_slider();
    QSlider& get_speed_two_slider();
    QSlider& get_size_one_slider();
    QSlider& get_size_two_slider();

signals:

public slots:

private:
    std::unique_ptr<QGridLayout> layout;

    std::unique_ptr<QLabel> speed_one_label;
    std::unique_ptr<QSlider> speed_one_slider;
    std::unique_ptr<QLabel> speed_two_label;
    std::unique_ptr<QSlider> speed_two_slider;

    std::unique_ptr<QLabel> size_one_label;
    std::unique_ptr<QSlider> size_one_slider;
    std::unique_ptr<QLabel> size_two_label;
    std::unique_ptr<QSlider> size_two_slider;
};

#endif // CONTROLPANEL_H
