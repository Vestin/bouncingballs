#ifndef PHYSICS_H
#define PHYSICS_H

#include "ball.h"

#include <vector>
#include <chrono>
#include <cmath>
#include <memory>

class Physics
{
public:
    Physics(std::vector<std::unique_ptr<Ball>>& balls, QVector2D& resolution);
    void update();
private:
    std::chrono::time_point<std::chrono::steady_clock> last_time;
    std::vector<std::unique_ptr<Ball>>& balls;
    QVector2D& resolution;

    void wall_bounce(Ball& ball);
    float get_adjusted_ball_radius(Ball& ball);
    bool do_balls_collide(Ball& ball_one, Ball& ball_two);
    void resolve_collision(Ball& ball_one, Ball& ball_two);
};

#endif // PHYSICS_H
