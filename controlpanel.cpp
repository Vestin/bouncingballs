#include "controlpanel.h"

ControlPanel::ControlPanel(QWidget *parent) : QWidget(parent)
{
    layout = std::unique_ptr<QGridLayout>(new QGridLayout(this));

    // Speed controls
    speed_one_label = std::unique_ptr<QLabel>(new QLabel("Prędkość pierwszej kulki"));
    layout->addWidget(speed_one_label.get(), 0, 0);

    speed_one_slider = std::unique_ptr<QSlider>(new QSlider(this));
    speed_one_slider->setOrientation(Qt::Horizontal);
    speed_one_slider->setRange(Ball::min_speed, Ball::max_speed);
    layout->addWidget(speed_one_slider.get(), 0, 1);

    speed_two_label = std::unique_ptr<QLabel>(new QLabel("Prędkość drugiej kulki"));
    layout->addWidget(speed_two_label.get(), 1, 0);

    speed_two_slider = std::unique_ptr<QSlider>(new QSlider(this));
    speed_two_slider->setOrientation(Qt::Horizontal);
    speed_two_slider->setRange(Ball::min_speed, Ball::max_speed);
    layout->addWidget(speed_two_slider.get(), 1, 1);

    // Size controls
    size_one_label = std::unique_ptr<QLabel>(new QLabel("Promień pierwszej kulki"));
    layout->addWidget(size_one_label.get(), 0, 2);

    size_one_slider = std::unique_ptr<QSlider>(new QSlider(this));
    size_one_slider->setOrientation(Qt::Horizontal);
    layout->addWidget(size_one_slider.get(), 0, 3);

    size_two_label = std::unique_ptr<QLabel>(new QLabel("Promień drugiej kulki"));
    layout->addWidget(size_two_label.get(), 1, 2);

    size_two_slider = std::unique_ptr<QSlider>(new QSlider(this));
    size_two_slider->setOrientation(Qt::Horizontal);
    layout->addWidget(size_two_slider.get(), 1, 3);

    setLayout(layout.get());
}

QSlider &ControlPanel::get_speed_one_slider()
{
    return *speed_one_slider;
}

QSlider &ControlPanel::get_speed_two_slider()
{
    return *speed_two_slider;
}

QSlider &ControlPanel::get_size_one_slider()
{
    return *size_one_slider;
}

QSlider &ControlPanel::get_size_two_slider()
{
    return *size_two_slider;
}
