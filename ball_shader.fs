#version 400 core
out vec4 color;
flat in int vertex_id;

void main(void)
{
	vec2 center_position = vec2(0.5, 0.5);
	float dist = distance(gl_PointCoord, center_position);
        float circle_clamped = 1.0 - smoothstep(0.45, 0.5, dist);

        float red  = min(1.0 - vertex_id, 1.0);
        float blue = max(0.0 + vertex_id, 0.0);

        color = vec4(red, 0.0, blue, circle_clamped);
}
