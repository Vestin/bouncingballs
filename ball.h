#ifndef BALL_H
#define BALL_H

#include <QObject>
#include <array>
#include <QVector2D>
#include <random>
#include <chrono>


class Ball: public QObject
{
    Q_OBJECT

public:
    static const int min_speed = 10;
    static const int max_speed = 1000;

    Ball(QVector2D& resolution);
    const QVector2D& get_position();
    const QVector2D& get_velocity();
    void set_position(const QVector2D& position);
    void set_velocity(const QVector2D& velocity);
    float get_size();
    float get_radius();
    float get_speed();
    int get_radius_percentage() const;
    void move(float delta_time);
    std::array<float, 3> get_float_array();
    QVector2D position;
    static void update_max_radius(QVector2D& new_resolution);
    void recalculate_radius();
    void set_radius_percentage(int percentage);
    void set_speed(int percentage);
    void flip_velocity(size_t index);

signals:
    void speed_updated(float speed);

private:
    static constexpr float min_radius = 1.0f;
    static float max_radius;

    float radius_magnitude = 0.5; // Make adjustable!

    float circumference;
    QVector2D velocity;

    QVector2D get_random_position(QVector2D resolution, int radius);
    float get_random_circumference();
    QVector2D get_random_direction();
    float get_random_speed();
};

#endif // BALL_H
