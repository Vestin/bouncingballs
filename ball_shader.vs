#version 400 core
in vec2 qt_Vertex;
in float circle_radius;
flat out int vertex_id;

void main(void)
{
        gl_Position = vec4(qt_Vertex, 0.0, 1.0);
        gl_PointSize = circle_radius;
        vertex_id = gl_VertexID;
}
