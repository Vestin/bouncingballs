#ifndef SCREEN_H
#define SCREEN_H

#include "ball.h"
#include "physics.h"

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShader>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QVector2D>

#include <string>
#include <fstream>
#include <sstream>
#include <memory>



class Screen : public QOpenGLWidget
{
    Q_OBJECT

public:
    Screen(QVector2D resolution);
    QVector2D pixels_to_gl_coord(const QVector2D& pixels);
public slots:
    void new_frame();
    std::unique_ptr<Ball>& get_ball(int index);
    void set_ball_speed(int index, int speed_magnitude);
    void set_ball_size(int index, int size);

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w, int h) override;
    QOpenGLFunctions *opengl;

private:
    QVector2D resolution;

    std::vector<std::unique_ptr<Ball>> balls;
    Physics physics{balls, resolution};

    GLuint circle_center;
    GLuint circle_radius;

    QOpenGLShaderProgram* q_shader_program;
    QOpenGLBuffer ball_buffer{QOpenGLBuffer::VertexBuffer};

    void init_ball_shader();
    std::string get_file_text(const std::string& filename);
};

#endif // SCREEN_H
